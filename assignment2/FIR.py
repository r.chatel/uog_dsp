# -*- coding: utf-8 -*-
"""
Created on Thu Oct 25 14:55:11 2018

@author: Remy CHATEL 2411062C
"""

import numpy as np
import matplotlib.pyplot as plt

class FIRFilter:
    taps = 0                                        # Number of taps of filter
    offset = 0                                      # Current offset of the buffer
    buffer = np.zeros(taps, dtype='float32')        # Value buffer
    coeffs = np.zeros(taps, dtype='float32')        # Filter's coefficients
    deltaF = 0                                      # Frequency resolution of the filter
    
    def __init__(self, coefficients):
        self.coeffs = coefficients
        self.taps   = len(self.coeffs)
        self.offset = 0
        self.buffer = np.zeros(self.taps)
        return
    
    def filter(self, value):
        """
        Filter the new value according to the filter's coefficients
        
        Parameters
        ----------
        value : float
            The new value to add to the buffer and to filter
        
        Returns
        -------
        output : float
            The filtered value
        """
        output   = 0.                               # Initialising the output
        i_coeff  = 0                                # Start index of coefficients
        i_buffer = self.offset                      # Start index of buffer
        self.buffer[i_buffer] = value               # Storing the new value in buffer
        
        while i_buffer >= 0:                        # First part of buffer
                                                    # Applying the filter
            output += self.buffer[i_buffer] * self.coeffs[i_coeff]
            i_coeff  += 1                           # Going through the coeffs
            i_buffer -= 1                           # Going through buffer backward
        
        i_buffer = self.taps -1                     # Going to the end of buffer
                                                    # to reach its second part
        while i_coeff < self.taps:                  # Second part of buffer
                                                    # Applying the filter
            output += self.buffer[i_buffer] * self.coeffs[i_coeff]
            i_coeff  += 1                           # Going through the coeffs
            i_buffer -= 1                           # Going through buffer backward
        
        self.offset += 1                            # Incrementing offset for next
                                                    # for next processing
        if self.offset >= self.taps:                # Offset must stay within buffer
            self.offset = 0
        
        return output
    
    def process(self, signal):
        """
        Apply the filter to an array
        
        Parameters
        ----------
        signal : numpy array
            The signal to be filtered
        """
        N = len(signal)
        output = np.zeros(N)
        for i in range(N):
            output[i] = self.filter(signal[i])
        return output
    
    def print(self, Fs, title='FIR Filter'):
        print("Number of taps : " + str(self.taps))
        f_axis = np.linspace(0, Fs, len(self.coeffs))
        fig, axs = plt.subplots(2,1, constrained_layout=True, figsize=(6.4*1.5,4.8*1.5))
        axs[0].plot(self.coeffs)
        axs[0].set_xlabel("Taps")
        axs[0].set_ylabel("Amlitude")
        axs[0].set_title("FIR Coefficients")
        axs[0].grid(True)
        axs[1].plot(f_axis, 20*np.log10(np.abs(np.fft.fft(self.coeffs))))
        axs[1].set_xlabel("Frequency (Hz)")
        axs[1].set_ylabel("Amlitude (dB)")
        axs[1].set_title("FIR Frequency response")
        axs[1].grid(True)
        fig.suptitle(title)
        return

class StdFilter:
    """
    A standard filter such as low pass, high pass, band pass or band stop filter
    """
    fir = FIRFilter(np.array([]))
    spectrum = np.array([])
    Fs = 0
    dF = 0.
    taps = 0
    
    def __init__(self, Fs, dF, filter_list, window='none'):
        """
        Constructor of the StdFilter object
        
        Parameters
        ----------
        Fs : float
            Sampling frequency of the recording
        dF : float
            Desired frequency resolution of the filter
        filter_list : list of tuples
            The list of ideal filter wanted in the filter on the following format :
            (type, cut-off frequency, bandwidth) or (type, cut-off frequency)
        window : str
            Type of window to apply to the filter, default : 'none'        
        """
        self.Fs = Fs
        self.dF = dF
        self.window = window
        self.taps = int(Fs/dF)
        if self.taps%2 != 0:
            self.taps += 1
        self.spectrum = np.ones(self.taps)
        
        for i in range(len(filter_list)):
            self.add_filter(*filter_list[i])
        
        M = self.taps                                   # Shorter alias for self.taps
        w = np.ones(M)                                  # Create and chose the window
        if window == "hamming":
            w = np.hamming(M)
        if window == "hanning":
            w = np.hanning(M)
        if window == "blackman":
            w = np.blackman(M)
        if window == "bartlett":
            w = np.bartlett(M)
        iresponse = np.fft.ifft(self.spectrum).real     # Computing the impulse response
        coefficients = np.zeros(M)                      # Creating the coeeficients array
        coefficients[:int(M/2)] = iresponse[int(M/2):]  # Shift lowest part to the right
        coefficients[int(M/2):] = iresponse[:int(M/2)]  # Shift highest part to the left
        coefficients = coefficients * w                 # Applying the window
        self.fir = FIRFilter(coefficients)              # Sending coeffs to FIRfilter
        
        return
    
    def add_filter(self, filter_type, f_c, B = 0):
        """
        Add a standard ideal filter to the current filter
        
        Parameters
        ----------
        filter_type : str
            Specify the type of the filter (low_pass, high_pass, band_stop,
            or band_pass)
        f_c : float
            Cut-off frequency of the filter
        B : float (optionnal)
            Width of the band for bandstop and bandpass filters
        """
        M = self.taps
        if filter_type == "low_pass":                   # For the low pass filter
            fresponse = np.zeros(M)                     # Create an array
            i_f = int(f_c/self.Fs*M)                    # Map frequency to index
            fresponse[:i_f]   = 1.                      # Set gain to 1 below cut off
            fresponse[M-i_f:] = 1.                      # Apply the miror over Fs/2
        if filter_type == "high_pass":                  # For the high pass filter
            fresponse = np.zeros(M)                     # Create an array
            i_f = int(f_c/self.Fs*M)                    # Map frequency to index
            fresponse[i_f:M-i_f] = 1.                   # Set gain to 1 over cut off
        if filter_type == "band_stop":                  # For the band stop
            fresponse = np.zeros(M)                     # Create an array
            i_low  = int((f_c - B/2.)/self.Fs*M)        # Map frequency to index
            i_high = int((f_c + B/2.)/self.Fs*M)
            fresponse[:i_low]          = 1.             # Set the gain
            fresponse[i_high:M-i_high] = 1.
            fresponse[M-i_low:]        = 1.
        if filter_type == "band_pass":                  # For the band pass filter
            fresponse = np.zeros(M)                     # Create an array
            i_low  = int((f_c - B/2.)/self.Fs*M)        # Map frequency to index
            i_high = int((f_c + B/2.)/self.Fs*M)
            fresponse[i_low:i_high]     = 1.            # Set the gain
            fresponse[M-i_high:M-i_low] = 1.
            
        self.spectrum = self.spectrum * fresponse
        return
    
    def filter(self, value):
        """
        Filter the new value according to the filter's coefficients
        
        Parameters
        ----------
        value : float
            The new value to add to the buffer and to filter
        
        Returns
        -------
        output : float
            The filtered value
        """
        output = self.fir.filter(value)
        return output
    
    def process(self, signal):
        """
        Apply the filter to an array
        
        Parameters
        ----------
        signal : numpy array
            The signal to be filtered
        """
        output = self.fir.process(signal)
        return output
    
    def print(self, Fs, title='Standard FIR Filter'):
        print('Sampling frequency : ' + str(self.Fs))
        print("Frequency resolution : " + str(self.dF))
        print("Window : " + self.window)
        self.fir.print(Fs, title)
        return