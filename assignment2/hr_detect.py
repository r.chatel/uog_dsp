# -*- coding: utf-8 -*-
"""
Created on Sun Nov  4 13:49:07 2018

@author: Remy CHATEL 2411062C
"""

import matplotlib.pyplot as plt
import numpy as np
import FIR as fir
import ECG as ecg
import timeit as timeit
plt.close('all')
print('------------------------------------------------------------')
print('ECG Filtering program')
print('Remove the 50Hz noise and the DC component and then extract')
print('the instantaneous heart rate of the ECG recording\n')

Fs = 1000
dF = 0.55
heart = ecg.ECG('ecg_2.dat', Fs)

print('Creating the FIR filter for 50Hz band stop and 1Hz high pass')
notch50 = fir.StdFilter(Fs, dF, [('high_pass', 1), ('band_stop', 50, 5)], 'hamming')
notch50.print(heart.Fs)
 
heart.print('Original ECG recording')
timestamp = timeit.time.time()
heart.process(notch50)
heart.match('inputs/good_wave.dat', 1/5)
time = 1000 * (timeit.time.time() - timestamp)/len(heart.dat)
heart.print('Cleaned ECG recording')

avg = np.average(heart.rates[2:])
maximum = np.max(heart.rates[2:])
print('\nMaximum instantaneous heart rate : %d'% maximum)
print('Average heart rate : %d'% avg)
print('Average exectution per sample : %1.2f ms'% time)

plt.figure(constrained_layout=True, figsize=(6.4*1.5,4.8*1.5))
plt.plot(heart.rates_time[2:], heart.rates[2:])
plt.plot(np.linspace(heart.rates_time[1], heart.rates_time[-1],len(heart.rates)-2),
                     avg * np.ones(len(heart.rates)-2))
plt.legend(('Instantaneous','Average'))
plt.xlabel('Time (s)')
plt.ylabel('Heart rate (bpm)')
plt.title('Instantaneous heart rate')
plt.grid(True)

print('------------------------------------------------------------')