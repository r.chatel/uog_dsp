# -*- coding: utf-8 -*-
"""
Created on Thu Nov  1 21:07:29 2018

@author: Remy CHATEL 2411062C
"""
import matplotlib.pyplot as plt
import numpy as np
import FIR as fir

class ECG:
    """
    A class that holds an ECG recording and can process it to clean it or extract the
    instantaneous heart rate
    """
    dat = np.array([])
    matched = np.array([])
    Fs = 0
    
    def __init__(self, filepath, Fs):
        self.dat = np.loadtxt(filepath)[:,3]
        self.Fs = Fs
        return
    
    
    def process(self, fir):
        """
        Applies a FIR filter to the recording
        
        Parameters
        ----------
        fir : FIR object
            The FIR filter that will be used on the recording
        """
        self.dat = fir.process(self.dat)
        return
    
    def match(self, pattern, threshold_factor):
        """
        Apply a matching filter to the recording to find the heartbeat pattern in the
        recording and deduce the heart rate from this output
        
        Parameters
        ----------
        pattern : tuple of int or str
            Either the couple of indexes that define the reference pattern or a filepath
            to an external reference
        threshold_factor : float
            The multiplying factor applied to the maximum value of the result of the 
            matched filter to define the detection threshold
        
        Yields
        ------
        Fills the rates array with the instantaneous heart rate
        """
        # Finding a template from an external recording or from a slice of the recording
        if type(pattern) == tuple:
            i_srt, i_end = pattern
            template = self.dat[i_srt:i_end]
        else:
            template = np.loadtxt(pattern)
        plt.figure()
        plt.plot(template)
        plt.xlabel('Taps')
        plt.ylabel('Amplitude')
        plt.title('Template')
        plt.grid(True)
        template = template[::-1]                           # Time reverse the template
        
        matched_filter = fir.FIRFilter(template)            # Create FIR with the template
        self.matched = matched_filter.process(self.dat)     # Process the signal
        self.matched = self.matched ** 2                    # Square to increase SNR
        
        threshold = np.max(self.matched) * threshold_factor # Define detection threshold
        
        length = len(self.matched)
        t_axis = np.linspace(0, length/self.Fs, length)     # Create a time axis
        l_threshold = threshold * np.ones(length)           # Plot the threshold
        plt.figure(constrained_layout=True, figsize=(6.4*1.5,4.8*1.5))
        plt.grid(True)
        plt.plot(t_axis, self.matched)
        plt.plot(t_axis, l_threshold)
        plt.xlabel('Time (s)')
        plt.ylabel('Amplitude')
        plt.title('Matched signal')
        
        max_list = []                                       # Create list for index of max
        flag = False                                        # Create a detection flag
        i_min, i_max = 0, 0                                 # Set indexes to zero
        for i in range(length):                             # Run through the data
            if not(flag) and self.matched[i]>threshold:     # On rising edge of a peak
                flag = True                                 # Set detection flag to true
                i_min = i                                   # Record index of start
            if flag and self.matched[i]<threshold:          # On faling edge of a peak
                flag = False                                # Set detection flag to false
                i_max = i                                   # Record index of end
                # Find the index of the maximum between i_min and i_max
                max_list.append(self.matched[i_min:i_max+1].argmax() + i)
       
        beats = np.array(max_list)                          # Convert to array
        self.rates = np.zeros(len(beats)-1)                 # Create rates array
        for i in range(len(self.rates)):                    # Populate the array
            # Compute the rates in bpm
            self.rates[i] = (60 * self.Fs) /(beats[i+1] - beats[i])
        
        self.rates_time = beats[1:] / self.Fs               # Creating the time axis
        """
        for i in range(len(self.rates)):                    # Weeding out weird rates
            if self.rates[i] < 30 or self.rates[i] >200:
                self.rates[i] = 0
        """
        return self.rates

    def print(self, title="ECG"):
        """
        Prints the recording in a orderly fashion
        """
        N = len(self.dat)
        L = N / self.Fs
        t_axis = np.linspace(0, L, N)
        f_axis = np.linspace(0, self.Fs, N)
        f_axis = f_axis[:int(len(f_axis)/2)]
        sig_fft = np.abs(np.fft.fft(self.dat))
        
        fig, axs = plt.subplots(2,1, constrained_layout=True, figsize=(6.4*1.5,4.8*1.5))
        axs[0].plot(t_axis, self.dat.real)
        axs[0].set_xlabel("Time (s)")
        axs[0].set_ylabel("Data amplitude")
        axs[0].set_title("Time domain data", loc='right')
        axs[0].grid(True)
        axs[1].plot(f_axis, 20*np.log10(sig_fft.real[:len(f_axis)]))
        axs[1].set_xlabel("Frequency (Hz)")
        axs[1].set_ylabel("Data amplitude (dB)")
        axs[1].set_title("Frequency domain data", loc='right')
        axs[1].grid(True)
        fig.suptitle(title)
        return