# -*- coding: utf-8 -*-
"""
Created on Sun Nov  4 13:49:07 2018

@author: Remy CHATEL 2411062C
"""

import matplotlib.pyplot as plt
import numpy as np
import FIR as fir
import ECG as ecg
import timeit as time
plt.close('all')
print('------------------------------------------------------------')
print('ECG Filtering program')
print('Remove the 50Hz noise and the DC component ')

Fs = 1000
dF = 0.5
heart = ecg.ECG('ecg_1.dat', Fs)

print('Creating the FIR filter for 50Hz band stop and 1Hz high pass')
notch50 = fir.StdFilter(Fs, dF, [('high_pass', 1), ('band_stop', 50, 5)], 'hamming')
notch50.print(heart.Fs)

heart.print('Original ECG recording')
timestamp = time.time.time()
heart.process(notch50)
exec_time = time.time.time() - timestamp
sample_time = exec_time/len(heart.dat) * 1000
sampling_period = 1000/Fs
heart.print('Cleaned ECG recording')
print('Total computation time : %2.3f s'% exec_time)
print('Computation time per sample : %1.2f ms'% sample_time)
print('Time between samples : %1.2f ms'% sampling_period)
print('------------------------------------------------------------')