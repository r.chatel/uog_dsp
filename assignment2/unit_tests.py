# -*- coding: utf-8 -*-
"""
Created on Thu Oct 25 18:04:54 2018

@author: Remy CHATEL 2411062C
"""
# ==============================================================================
# Tools
# ==============================================================================
import FIR as fir
import ECG as ecg
import numpy as np
import matplotlib.pyplot as plt
plt.close('all')

def print_sig(signal, Fs, L):
    N = len(signal)
    t_axis = np.linspace(0, L, N)
    f_axis = np.linspace(0, Fs, N)
    sig_fft = np.abs(np.fft.fft(signal))
    
    plt.subplots(2,1, figsize=(6.4*1.5,4.8*1.5))
    plt.subplot(2,1,1)
    plt.plot(t_axis, signal.real)
    # plt.plot(signal.real)
    plt.xlabel("Time (s)")
    plt.ylabel("Data amplitude")
    plt.grid(True)
    plt.title("Time domain data", loc='left')
    
    plt.subplot(2,1,2)
    plt.plot(f_axis[:int(len(f_axis)/2)], sig_fft.real[:int(len(f_axis)/2)])
    plt.xlabel("Frequency (Hz)")
    plt.ylabel("Data amplitude")
    plt.yscale('log')
    plt.grid(True)
    plt.title("Frequency domain data", loc='left')
    return
# ==============================================================================
# Unit test for FIRFilter (cumulative)
# ==============================================================================

c = [1,5,8,-3,5,7,12]
filter1 = fir.FIRFilter(c)
v = np.zeros(3 * len(c))
v[1] = 1
out = np.zeros(len(v))

print("Impulse response testing : ")

for i in range(len(v)):
    out[i] = filter1.filter(v[i])
    
print("Data in : ")
print(v)
print("Data out :")
print(out)

# ==============================================================================
# Unit test for FIRFilter (cumulative)
# ==============================================================================

c = [1,5,8,-3,5,7,12]
filter1 = fir.FIRFilter(c)
v = np.zeros(3 * len(c))
v[len(c):] = 1
out = np.zeros(len(v))

print("Cumulative sum of coefficients")

for i in range(len(v)):
    out[i] = filter1.filter(v[i])
    
print("Data in : ")
print(v)
print("Data out :")
print(out)

# ==============================================================================
# Unit test for StdFilter
# ==============================================================================
"""
duration = 8
Fs = 1000
dF = 0.5
n_sample = duration * Fs
t = np.linspace(0,duration, n_sample)
sig = np.sin(2*np.pi*50*t) + np.sin(2*np.pi*t) + 3

filter1 = fir.StdFilter(Fs, dF, [('high_pass', 1), ('band_stop', 50, 5)], 'hamming')
filter1.print(Fs)

out = np.zeros(n_sample)

for i in range(n_sample):
    out[i] = filter1.filter(sig[i])

print_sig(sig, Fs, duration)
print_sig(out, Fs, duration)
# print_sig(out[int(len(out)/2):], Fs, duration/2)
"""
# ==============================================================================
# Unit test on ECG
# ==============================================================================
"""
dat = np.loadtxt('inputs/ecg2.dat')[10000:35000,3]
# M = int(len(dat)/10)
# dat = dat[M:M*5]
Fs = 1000
dF = 0.5
duration = len(dat)/Fs
print_sig(dat, Fs, duration)

dat_out = np.copy(dat)

filter1 = fir.StdFilter(Fs, dF, [('high_pass', 1), ('band_stop', 50, 5)], 'hamming')
filter1.print(Fs)
dat_out = filter1.process(dat_out)

print_sig(dat_out, Fs, duration)

template = dat_out[10000:10700]
plt.figure()
plt.plot(template)
template = template[::-1]
plt.plot(template)
matched = fir.FIRFilter(template)
matched.print(Fs)

dat_matched = matched.process(dat_out)
dat_matched = dat_matched**2
plt.figure()
plt.grid(True)
plt.plot(np.linspace(0, duration, len(dat_matched)), dat_matched)
"""
# ==============================================================================
# Unit test print function
# ==============================================================================
"""
x = np.arange(-200,201)
h = (np.sin(45/500*2*np.pi*x)-np.sin(55/500*2*np.pi*x)) / (x*np.pi)
h[200] = 1-(2/50)
w = np.ones(len(x))
w = np.blackman(len(x))
filter1 = fir.FIRFilter(h*w)
filter1.print(500, 'Test')
"""
# ==============================================================================
# Unit test on windows
# ==============================================================================
"""
Fs = 1000
F_c = 50
B = 10
filter1 = fir.StdFilter(Fs, dF, [('high_pass', 1), ('band_stop', 50, 5)], 'hamming')
filter1.print(Fs)
filter1 = ffir.StdFilter(Fs, dF, [('high_pass', 1), ('band_stop', 50, 5)], 'hanning')
filter1.print(Fs)
filter1 = fir.StdFilter(Fs, dF, [('high_pass', 1), ('band_stop', 50, 5)], 'blackman')
filter1.print(Fs)
filter1 = fir.StdFilter(Fs, dF, [('high_pass', 1), ('band_stop', 50, 5)], 'bartlett')
filter1.print(Fs)
"""
# ==============================================================================
# ECG Printer All channel
# ==============================================================================
"""
dat = np.loadtxt('inputs/ecg4.dat')[10000:35000,:]
Fs = 1000
duration = len(dat)/Fs
plt.subplots(2,1)
plt.subplot(2,1,1)
plt.plot(dat[:,2])
plt.subplot(2,1,2)
plt.plot(dat[:,3])
"""
# ==============================================================================
# ECG Printer playback
# ==============================================================================
"""
dat = np.loadtxt('inputs/matched.dat')
Fs = 1000
duration = len(dat)/Fs
plt.figure()
plt.plot(np.linspace(0,duration, len(dat)), dat)
plt.xlabel('Time (s)')
plt.ylabel('Amplitude')
"""
# ==============================================================================
# Matched signal processing
# ==============================================================================
"""
dat = np.loadtxt('inputs/matched.dat')[2500:22500]
Fs = 1000
threshold = np.max(dat)/3

t_axis = np.linspace(0, len(dat)/Fs, len(dat))
l_threshold = threshold * np.ones(len(dat))

plt.figure()
plt.grid(True)
plt.plot(t_axis, dat)
plt.plot(t_axis, l_threshold)

max_list = []
flag = False
i_min, i_max = 0, 0
for i in range(len(dat)):
    if not(flag) and dat[i]>threshold:
        flag = True
        i_min = i
    if flag and dat[i]<threshold:
        flag = False
        i_max = i
        max_list.append(dat[i_min:i_max+1].argmax() + i)

beats = np.array(max_list) / Fs

rates = np.zeros(len(beats)-1)
for i in range(len(rates)):
    rates[i] = 60/(beats[i+1] - beats[i])

print(rates)
"""
# ==============================================================================
# Unit tests on ECG class
# ==============================================================================
"""
Fs = 1000
dF = 0.5
heart = ecg.ECG('inputs/ecg3.dat', Fs)
# heart.dat = heart.dat[10000:35000]

notch50 = fir.StdFilter(Fs, dF, [('high_pass', 1), ('band_stop', 50, 5)], 'hamming')
notch50.print(heart.Fs)
 
heart.print()
heart.process(notch50)
heart.print()

heart.match('inputs/good_wave.dat', 1/10)

plt.figure()
plt.plot(heart.rates)
"""
# ==============================================================================
# To Dos
# ==============================================================================
# -> Check out the gain of the USB-DUX/ADC/Pre-amp
# -> Is real time worth it ?
# -> Do I have to actually look up for the template in the program or can I save it and
#    staticaly use it ?
# -> Is ecg_2.dat a clean ecg or or raw