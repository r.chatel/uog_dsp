# -*- coding: utf-8 -*-
"""
Created on Wed Oct 10 09:09:19 2018
@author: Remy CHATEL 2411062C
@author: Chiemeka AGUWAMBA 2409785A
"""

import voice
import word
import numpy as np
import matplotlib.pyplot as plt

plt.close('all')

def clean(data):
    data.setDC(0)                       # Removing the DC component of the signal
    data.cut_band(    8e3 , data.Fs/2)  # Low pass filter above the sound card cutoff
    data.cut_band(     0  , 100      )  # High pass filter under 150Hz to remove bass noise
    data.gaindB(- 3, 2.5e3, 4e3      )
    data.gaindB(-12,  5e3 , 7e3      )
    data.update()
    return data

def read(filepath, width, threshold):
    wordin = word.Word(clean(voice.Voice("inputs/" + filepath)))
    wordin.trim_to_word(threshold)
    wordin.slicing(width)
    wordin.get_fundamentals()
    print("\n" + filepath + ":")
    wordin.print('outputs/' + filepath[:-4])
    return wordin


chunk_w = .350
a_min = 3000

paths = ["launch_r0.wav", "launch_r2.wav", "abort_r0.wav", "abort_r1.wav", "abort_r2.wav" ]
words = []
for i in range(len(paths)):
    words.append(read(paths[i], chunk_w, a_min))

sentence = read("sentence_r.wav", chunk_w/2, a_min-1000)


ref_abort = np.array([490,150])
ref_launch = np.array([385, 160])
print("\n")
print("Matching with \"abort\" : ")
print(sentence.compare(ref_abort, 50))
print("Matching with \"launch\" : ")
print(sentence.compare(ref_launch, 50))
plt.show()
plt.close('all')

