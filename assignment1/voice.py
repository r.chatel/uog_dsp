# -*- coding: utf-8 -*-
"""
Created on Sat Oct 13 18:51:10 2018
@author: Remy CHATEL 2411062C
@author: Chiemeka AGUWAMBA 2409785A
"""
import numpy as np
import matplotlib.pyplot as plt
import scipy.io.wavfile as wave

class Voice:
    """
    A class that represent a voice sample and enable transformations on it
    """
    signal  = np.array([], dtype='float32') # The signal representing the voice sample
    sig_fft = np.array([], dtype='float32') # The FFT of the voice sample
    t_axis  = np.array([], dtype='float32') # A time axis for plotting
    f_axis  = np.array([], dtype='float32') # A frequency axis for plotting and mapping
    L = 0                                   # The number of index in the sample
    Fs = 0.                                 # The sampling frequency of the sample
    hasChanged = 0                          # Keeping track of modifications
    
    def __init__(self, filepath):
        """
        Import a voice sample from a wave file
        
        Parameters
        ----------
        filepath : str
            The path to the file to read
        """
        self.Fs, sig = wave.read(filepath)              # Reading file
        self.signal = sig[:,0]                          # Selecting channel 1
        self.L = len(self.signal)                       # Reading the length
        self.t_axis = np.linspace(0, self.L/self.Fs, self.L)
        self.f_axis = np.linspace(0,        self.Fs, self.L)
        self.sig_fft = np.fft.fft(self.signal)          # Applying FFT
        return
    
    def update(self):
        """
        Update the signal with the current FFT
        """
        if self.hasChanged:                              # Updating only if changed
            self.signal = np.fft.ifft(self.sig_fft)
            self.hasChanged = 0
        return
    
    def gaindB(self, gain, f_low, f_high):
        """
        Apply a gain (in dB) to a frequency domain signal between two boundaries
        frequencies.
        
        Parameters
        ----------
        gain : dtype
            The amplification gain. Should be of the same type as the data array
        f_min : float
            Low cut-off frequency of the amplification
        f_max : float
            High cut-off frequency of the amplification
            
        Yields
        ------
        The FFT of the signal is now modified according to this gain
        """
        i_min = int(self.L * f_low / self.Fs)           # Mapping index to frequencies
        i_max = int(self.L * f_high/ self.Fs)
        gain = 10**(gain/20)                            # Converting to linear
        
        for i in range(i_min, i_max+1):                 # Applying gain to spectrum
            self.sig_fft[i] = self.sig_fft[i]*gain      # before F_sampling/2
    
        if i_min == 0:
            self.sig_fft[-1] = self.sig_fft[-1]*gain    # aking care of the zero index
            i_min += 1
    
        for i in range(self.L-i_max,self.L-i_min+1):    # Applying gain to the 
            self.sig_fft[i] = self.sig_fft[i]*gain      # spectrum after F_sampling/2
        self.hasChanged = 1
        return
    
    def setDC(self, value):
        """
        Set the value of the DC component of the specturm of the signal
        
        Parameters
        ----------
        value : float32
            The value to write on the DC component of the specturm
        
        Yields
        ------
        The value of the DC component has been changed
        """
        self.sig_fft[0] = value
        return
    
    def cut_band(self, f_low, f_high):
        """
        Apply a cut-band filter on the FFT of the signal between the given frequencies
        
        Parameters
        ----------
        f_low : float
            The low cut-off frequency of the filter
        f_high : float
            The high cut-off frequency of the filter
        
        Yields
        ------
        The FFT has been filtered by setting all values within the boundaries
        to zero
        """
        i_min = int(self.L * f_low / self.Fs)           # Mapping index to frequencies
        i_max = int(self.L * f_high/ self.Fs)
        gain = 0                                        # Setting a linear gain to 0
        
        for i in range(i_min, i_max+1):                 # Applying gain to spectrum
            self.sig_fft[i] = self.sig_fft[i]*gain      # before F_sampling/2
    
        if i_min == 0:
            self.sig_fft[-1] = self.sig_fft[-1]*gain    # aking care of the zero index
            i_min += 1
    
        for i in range(self.L-i_max,self.L-i_min+1):              # Applying gain to the 
            self.sig_fft[i] = self.sig_fft[i]*gain      # spectrum after F_sampling/2
        self.hasChanged = 1
        return
    
    def print(self, fig_title):
        """
        Print the signal and its FFT
        
        Parameters
        ----------
        fig_title : str
            The name of the figure
        
        Yields
        ------
        Produces two figures stored in the 'outputs' folder, one of the signal 
        and its FFT.
        """
        self.update()                                   # Updating the signal
        plt.subplots(2,1, figsize=(6.4*1.5,4.8*1.5))
        plt.subplot(2,1,1)
        plt.plot(self.t_axis, self.signal.real)
        plt.xlabel("Time (s)")
        plt.ylabel("Data amplitude")
        plt.grid(True)
        plt.title("Time domain data", loc='left')
        
        plt.subplot(2,1,2)
        plt.plot(self.f_axis[:self.L//2], np.abs(self.sig_fft.real[:self.L//2]))
        plt.xlabel("Frequency (Hz)")
        plt.ylabel("Data amplitude")
        plt.yscale('log')
        plt.grid(True)
        plt.title("Frequency domain data", loc='left')
        
        plt.suptitle(fig_title, fontsize=16)
        plt.savefig(fig_title+'.pdf', transparent=True, format='pdf')
        return
    
    def write(self, filename='improved'):
        """
        Write a int16 coded signal array into a normalised float32 wav file
        
        Parameters
        ----------
        signal : numpy array
            The signal to write
        Fs : float
            Sampling frequency
        
        Yields
        ------
        Produces a wav file in the output folder containing a 2-channel, float32,
        normalised signal
        """
        self.update()                                   # Updating the signal
        # Making a 2-channel signal
        wave_out = np.zeros((self.L, 2), dtype='float32')
        wave_out[:,0] = self.signal.real[:]
        wave_out[:,1] = self.signal.real[:]
    
        # Converting audio signal to float32 normalised
        wave_out_float = np.zeros((self.L,2), dtype='float32')
        for i in range(self.L):
            wave_out_float[i] = wave_out.real[i]/32768.
        
        # Writing into a wav file
        wave.write(filename+'.wav', self.Fs, wave_out_float)
        return