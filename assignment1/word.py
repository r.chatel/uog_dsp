# -*- coding: utf-8 -*-
"""
Created on Mon Oct 15 00:36:20 2018
@author: Remy CHATEL 2411062C
@author: Chiemeka AGUWAMBA 2409785A
"""
import numpy as np
import syllable

class Word:
    syllables = []                              # List containing the syllables
    fundamentals = np.array([], dtype='int')    # List of syllables fundamental
    length = 0
    
    def __init__(self, voice_sample):
        self.syllables = []
        self.audio = voice_sample               # Storing the audio sample
        self.length = self.audio.L / self.audio.Fs
        return

    def trim_to_word(self, threshold):
        """
        Removes to blanks before and after the word
        
        Parameters
        ----------
        threshold : int16
            The detection threshold (in term of amplitude of int16) for the begin
            and end of the word
        
        Yields 
        ------
        The signal is limited to the actual word
        """
        i = 0                                   # Let's start from 0
        end = self.audio.L                      # Making sure we stay within the array
        while(self.audio.signal[i]<threshold and i< end):
            i += 1                              # Going through array until threshold
                                                # is triggered
        i -= 100                                # Going back a little
        self.audio.signal = self.audio.signal[i:]# Trimming the front of the sample
        i = len(self.audio.signal)-1            # Let's go backward from the end
        while(self.audio.signal[i]<threshold and i >= 0):
            i -= 1                              # Going backward until treshold
        i = max([0, i-500])                     # Taking a little bit more
        self.audio.signal = self.audio.signal[:i]# Trimming the end of the sample
        self.audio.L = len(self.audio.signal)   # Setting the new Voice variables
        self.audio.t_axis = np.linspace(0, self.audio.L/self.audio.Fs, self.audio.L)
        self.audio.f_axis = np.linspace(0,              self.audio.Fs, self.audio.L)
        self.audio.sig_fft = np.fft.fft(self.audio.signal)
        self.length = self.audio.L / self.audio.Fs
        return
    
    def slicing(self, width):
        """
        Slices the word into fixed width chunks
        
        Parameters
        ----------
        width : float32
            The required width (in seconds) of the syllable
        
        Yields
        ------
        Slices the audio sample into chunks that will be identified as syllables
        """ 
        index_width = int(width*self.audio.Fs) # Converting duration into list width
        i1 = 0
        i2 = index_width
        while(i2 < self.audio.L):   # Slicing chunk until the last one too short
            new_syllable = syllable.Syllable(self.audio.signal[i1:i2], self.audio.Fs)
            self.syllables.append(new_syllable) # Adding the chunk to the list
            i1 += index_width       # Then adding the last chunk
            i2 += index_width
        new_syllable = syllable.Syllable(self.audio.signal[i1:], self.audio.Fs)
        self.syllables.append(new_syllable)
        return
    
    def get_fundamentals(self):
        """
        Creates a table with the fundatmental of each chunk/syllable of the word
        
        Should use the Syllable.fundatmental() function on each chunk
        
        Yields
        ------
        Fills the syllable frequency list
        """
        self.fundamentals = np.zeros(len(self.syllables), dtype='int')
        for i in range(len(self.syllables)): # Retrieving the fundamental of syllables
            self.fundamentals[i] = int(self.syllables[i].fundamental())
        return
    
    def compare(self, reference, tolerance):
        """
        Compares the fundamentals of the current word with those of the reference
        word
        
        Parameters
        ----------
        reference : numpy array
            The reference word's array of fundatemental frequency of the syllables
        tolerance : float
            The frequency tolerance for the matching
        
        Returns
        -------
        isSame : boolean
            Returns true is the matching is successful and false otherwise
        """
        isSame = False                          # Initialising the boolean
        length = len(reference)    # Getting the length of ref
        if length == len(self.fundamentals):    # Taking care of equal length
            test_array = np.abs(self.fundamentals-reference)
            # print(test_array)
            if np.max(test_array) < tolerance:
                isSame = True
        else:                                   # Otherwise, roll through list
            for i in range(len(self.fundamentals)-length):# Dealing with variable width
                # making the array of the difference between the ref and the word
                test_array = np.abs(self.fundamentals[i:i+length]-reference)
                if np.max(test_array) < tolerance:  # If all freq are within tolerance
                    isSame = True                   # Set the boolean to True
        return isSame
    
    def print(self, figtitle):
        """
        Print and plot the data contained in the Word
        """
        self.audio.print(figtitle)
        print(self.fundamentals)
        print("Length : " + str(self.length) +" s")
        return