# -*- coding: utf-8 -*-
"""
Created on Fri Oct 19 16:37:58 2018
@author: Remy CHATEL 2411062C
@author: Chiemeka AGUWAMBA 2409785A
"""
import voice
import word
"""
--------------------------------------------------------------------------------
                                Class Definition
--------------------------------------------------------------------------------
"""
class Alexa:
    """
    A class to compare a sample word against a library of two words
    
    Its functions can clean, prepare and compare words
    """
    # Setting the width of syllable and the level threshold
    width = .350
    threshold = 3000
    tolerance = 50
    
    def clean(data):
        """
        Applies a set of filter and transformations to clean up the signal before
        processing
        
        Parameters
        ----------
        data : Voice object
            The voice sample to clean
        
        Returns
        -------
        data : Voice object
            The processed data
        """
        data.setDC(0)                       # Removing the DC component of the signal
        data.cut_band(    8e3 , data.Fs/2)  # Low pass filter above the sound card cutoff
        data.cut_band(     0  , 100      )  # High pass filter under 150Hz to remove bass noise
        data.gaindB(- 3, 2.5e3, 4e3      )
        data.gaindB(-12,  5e3 , 7e3      )
        data.update()
        return data
    
    def read(filepath, width, threshold):
        """
        Imports, clean and prepare a wav audio sample for further processing
        
        Parameters
        ----------
        filepath : str
            The relative path to the file
        width : float32
            The width (in s) of the slices representing a syllable
        threshold : int16
            The threshold to detect the begining and end of the word
        
        Returns
        -------
        wordin : Word object
            A word object containing the characterized data for further processing
        """
        wordin = word.Word(Alexa.clean(voice.Voice(filepath)))
        wordin.trim_to_word(threshold)
        wordin.slicing(width)
        wordin.get_fundamentals()
        return wordin
    
    def __init__(self, filepath):
        """
        Import all relevant files and prepare them for processing : the library
        files and the test file
        
        Parameters
        ----------
        filepath : str
            The relative path to the wav file that will be tested
        """
        # Importing reference words
        self.ref1 = Alexa.read("inputs/launch.wav", self.width, self.threshold)
        self.ref2 = Alexa.read("inputs/abort.wav" , self.width, self.threshold)
        # Importing test word/sentence with a less stringent threshold
        self.test = Alexa.read(  filepath  , self.width, self.threshold-1000)
        
        return
    
    def process(self):
        """
        Compare the imported word to the two library words
        
        Returns
        -------
        string : str
            A string containing the matched word(s)
        """
        string = ""
        if(self.test.compare(self.ref1.fundamentals, self.tolerance)):
            string += "Launch\t"
        if(self.test.compare(self.ref2.fundamentals, self.tolerance)):
            string += "Abort\t"
        if(string == ""):
            string = "No match"
        return string

"""
--------------------------------------------------------------------------------
                                Main Program
--------------------------------------------------------------------------------
"""

print("\n\n")
print("----------- \"Alexa ligth\" program -----------")
print("The following words make the library against")
print("which the input word will be compared")
print("\t - word 1: \"launch\" from \"launch.wav\"")
print("\t - word 1: \"abort\" from \"abort.wav\"")
question = "Detected word : "

print("\nTesting \"launch\" against the library")
alexa = Alexa("inputs/launch.wav")
print(question, end="\t")
print(alexa.process())

print("\nTesting \"abort\" against the library")
alexa = Alexa("inputs/abort.wav")
print(question, end="\t")
print(alexa.process())

print("\nTesting glasgow.wav against the library")
alexa = Alexa("inputs/glasgow.wav")
print(question, end="\t")
print(alexa.process())

print("\nTesting another \"abort\": abort_test.wav")
alexa = Alexa("inputs/abort_test.wav")
print(question, end="\t")
print(alexa.process())