# -*- coding: utf-8 -*-
"""
Created on Sun Oct 14 23:22:08 2018
@author: Remy CHATEL 2411062C
@author: Chiemeka AGUWAMBA 2409785A
"""

import numpy as np

class Syllable:
    sample  = np.array([], dtype='float32') # The signal representing the voice sample
    fft = np.array([], dtype='float32') # The FFT of the voice sample
    t_axis  = np.array([], dtype='float32') # A time axis for plotting
    f_axis  = np.array([], dtype='float32') # A frequency axis for plotting and mapping
    L = 0                                   # The number of index in the sample
    duration = 0.                           # The duration of the sample
    Fs = 0.                                 # The sampling frequency of the sample
    i_fundamental = 0                       # Index     of the fundamental
    f_fundamental = 0.                      # Frequency of the fundamental
    A_fundamental = 0                       # Amplitude to the fundamental
    
    def __init__(self, signal, Fsampling):
        self.sample = signal
        self.Fs = Fsampling
        self.L = len(signal)
        self.duration = self.L / self.Fs
        self.t_axis = np.linspace(0, self.L/self.Fs, self.L)
        self.f_axis = np.linspace(0,        self.Fs, self.L)
        self.fft = np.fft.fft(self.sample)
        return
    
    def fundamental(self):
        """
        Find the fundamental frequency (as frequency of maximum amplitude)
        """
        self.f_fundamental = 0                                  # Init variables
        self.i_fundamental = 0                                  
        a_max = np.max(np.abs(self.fft[:self.L//2]))            # Find the max value
        for k in range(1,self.L//2):                            # Read the array
            if np.abs(self.fft[k]) == a_max:                    # Until max is found
                self.i_fundamental = k                          # Record index of max
        self.f_fundamental = self.i_fundamental*self.Fs/self.L  # Map k to the Frequency axis
        self.A_fundamental= np.abs(self.fft[self.i_fundamental])# Record max amplitude
        return self.f_fundamental







