# -*- coding: utf-8 -*-
"""
Created on Wed Oct  3 09:06:27 2018

@author: Remy CHATEL 2411062C
"""

import numpy as np
import matplotlib.pyplot as plt
import scipy.io.wavfile as wave

def gain(frequencies, data, gain, f_min, f_max):
    """
    Apply a linear gain to a frequency domain signal between two boundaries
    frequencies.
    
    Parameters
    ----------
    frequencies : numpy array of float
        A linear frequency axis (sorted by ascending values) from 0Hz to
        F_sampling
    data : numpy array of dtype
        The dataset in the frequency domain to be amplified
    gain : dtype
        The amplification gain. Should be of the same type as the data array
    f_min : float
        Low cut-off frequency of the amplification
    f_max : float
        High cut-off frequency of the amplification
    
    Returns
    -------
    d_out : numpy array of dtype
        A new array with the amplified data in a frequency domain
    """
    d_out = np.copy(data)                       # Creating a new array
    i_min, i_max = 0, 0                         # Init of indexes
    
    while(frequencies[i_min]<f_min):            # Search of index for f_min
        i_min = i_min + 1
    while(frequencies[i_max]<f_max):            # Search of index for f_max
        i_max = i_max + 1
        
    for i in range(i_min, i_max):               # Applying gain to the spectrum
        d_out[i] = d_out[i]*gain                # before F_sampling/2
        
    for i in range(len(data)-i_max+1,len(data)-i_min+1):# Applying gain to the 
        d_out[i] = d_out[i]*gain                # spectrum after F_sampling/2
    return d_out

def test_real(array):
    test = np.array([])
    for i in range(len(array)):
        test = np.append(test, array[i].imag/array[i].real)
    if test.all()<1:
        print("Real enough")
    else:
        print("Imaginary part unreasonably large")
    return

def find_peaks(array, threshold):
    """
    Find the index of peaks over the threshold on a dataset
    
    Parameters
    ----------
    array : numpy array of dtype
        The dataset to process. A spiky signal is expected.
    threshold : dtype
        The threshold above which indexes must be recorded
    
    Returns
    -------
    i_peaks : numpy array of int
        The indexes of values above the threshold
    
    """
    i_peaks = np.array([], dtype='int')
    for i in range(len(array)):
        if array[i]>threshold:
            i_peaks = np.append(i_peaks, i)
    return i_peaks


"""
-------------------------------------------------------------------------------
Computation
-------------------------------------------------------------------------------
"""
Fs, data = wave.read('Glasgow.wav')            # Importing data
data = data[:,0]                                # Selecting channel 1
duration = len(data)/Fs                         # computing the duration
time_axis = np.linspace(0,duration, len(data))  # creating a time axis

data_freq = np.fft.fft(data)                    # Applying FFT to the data
freq_axis = np.linspace(0, Fs, len(data))       # Creating a frequency axis

data_mod = gain(freq_axis,data_freq,0,9e3,24e3) # Flatenning the spectrum
data_mod = gain(freq_axis, data_mod,0,49,51)    # Getting rid of the 50Hz noise
data_out = np.fft.ifft(data_mod)                # Returning to time domain

"""
-------------------------------------------------------------------------------
Plotting and outputs
-------------------------------------------------------------------------------
"""
"""
plt.figure()
plt.plot(time_axis, data)
plt.xlabel("Time (s)")
plt.ylabel("Data amplitude")
plt.title("Time domain data")
plt.savefig('time_domain.pdf', transparent=True)

plt.figure()
plt.plot(freq_axis, np.abs(data_freq))
plt.yscale('log')
plt.xlabel("Frequency (Hz)")
plt.ylabel("Data amplitude")
plt.title("Frequency domain data")
plt.savefig('freq_domain.pdf', transparent=True)

plt.figure()
plt.plot(freq_axis, np.abs(data_mod))
plt.yscale('log')
plt.xlabel("Frequency (Hz)")
plt.ylabel("Data amplitude")
plt.title("Frequency domain of modified data")

"""


"""
-------------------------------------------------------------------------------
Looking for parasitic imaginary part frequencies
-------------------------------------------------------------------------------
"""
imaginary = np.abs(np.fft.fft(data_out.imag))

peaks = find_peaks(imaginary, 10)*Fs/len(imaginary)
print(peaks)

plt.figure()
plt.plot(time_axis, data_out.imag)
plt.xlabel("Time (s)")
plt.ylabel("Data amplitude")
plt.title("Time domain of modified data")

plt.figure()
plt.plot(freq_axis[:len(freq_axis)//2], imaginary[:len(freq_axis)//2])

"""
Al right, so with the current functions, I have a non-zero imaginary part in my output signal.
Doing the Fourier transform of the imaginary part of the output signal yields three peaks :
    49.21Hz
    51.04Hz
    9000.Hz
We recongnise here the cutoff frequencies of our gain() function
In the time domain, it shows as a amplitude modulation of the two signals with 
a 100 factor between the two signals
"""


# wave.write('modified.wav', Fs, data_out.real)