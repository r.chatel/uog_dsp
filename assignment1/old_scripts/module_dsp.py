# -*- coding: utf-8 -*-
"""
Created on Wed Oct  3 17:04:14 2018

@author: Remy CHATEL 2411062C
"""
import numpy as np
import scipy.io.wavfile as wave

def gain(frequencies, data, gain, f_min, f_max, scale='lin'):
    """
    Apply a gain to a frequency domain signal between two boundaries
    frequencies.
    
    Parameters
    ----------
    frequencies : numpy array of float
        A linear frequency axis (sorted by ascending values) from 0Hz to
        F_sampling
    data : numpy array of dtype
        The dataset in the frequency domain to be amplified
    gain : dtype
        The amplification gain. Should be of the same type as the data array
    f_min : float
        Low cut-off frequency of the amplification
    f_max : float
        High cut-off frequency of the amplification
    scale : str
        Either linear ('lin') or decibell ('dB') gain
    
    Returns
    -------
    d_out : numpy array of dtype
        A new array with the amplified data in a frequency domain
    """
    d_out = np.copy(data)                       # Creating a new array
    N = len(frequencies)
    F_samp = frequencies[-1]
    i_min = int(N * f_min / F_samp)             # Mapping index to frequencies
    i_max = int(N * f_max / F_samp)
    
    if scale == 'dB':
        gain = 10**(gain/20)
    
    for i in range(i_min, i_max+1):             # Applying gain to the spectrum
        d_out[i] = d_out[i]*gain                # before F_sampling/2

    if i_min == 0:
        d_out[-1] = d_out[-1]*gain
        i_min += 1

    for i in range(len(data)-i_max,len(data)-i_min+1): # Applying gain to the 
        d_out[i] = d_out[i]*gain                # spectrum after F_sampling/2
    return d_out


def correlation(signal1, signal2):
    """
    Compute the cross-correlation of two datasets
    
    Parameters
    ----------
    signal1 : numpy array
        First signal of length L
    signal2 : numpy array
        Second signal of length at least L
    
    Returns
    -------
    corr : numpy array
        The cross correlation of the two signals for all indexes of signal 2
    
    Notes
    -----
    Signal 2 can be longer than signal 1, which can be used to find a "short"
    signal (1) into a "longer" dataset (2)
    """
    corr = np.zeros(len(signal2))
    for n in range(len(signal2)):
        for m in range(len(signal1)):
            corr[n] = np.conj(signal1)[m] * signal2[(n+m)%len(signal2)]
    return corr

def find_peaks(array, threshold):
    """
    Find the index of peaks over the threshold on a dataset
    
    Parameters
    ----------
    array : numpy array of dtype
        The dataset to process. A spiky signal is expected.
    threshold : dtype
        The threshold above which indexes must be recorded
    
    Returns
    -------
    i_peaks : numpy array of int
        The indexes of values above the threshold
    """
    i_peaks = np.array([], dtype='int')         # Creating the peak index array
    for i in range(len(array)):                 # Checking all values
        if array[i]>threshold:                  # If above the threshold
            i_peaks = np.append(i_peaks, i)     # registers it as a peak
    return i_peaks

def preprocessing(signal, Fs, F_low, F_high):
    """
    Pre-process the signal to remove unwanted noise outside of the cut off
    frequencies of the sound card or microphone
    """
    freq_axis = np.linspace(0, Fs, len(signal))     # Creating a frequency axis
    data_freq = np.fft.fft(signal)                  # Applying FFT to the data
    data_mod = np.copy(data_freq)                   # Creating a new array
    data_mod[0] = 0                                 # Removing DC component
    # Removing data outside of cutoff frequencies of the sound card/microphone
    data_mod = gain(freq_axis, data_mod, 0, F_high, Fs/2 )
    data_mod = gain(freq_axis, data_mod, 0,   0   , F_low)
    data_out = np.fft.ifft(data_mod)                # Returning to time domain
    return data_out


def write_floatN(signal, Fs):
    """
    Write a int16 coded signal array into a normalised float32 wav file
    
    Parameters
    ----------
    signal : numpy array
        The signal to write
    Fs : float
        Sampling frequency
    """
    # Making a 2-channel signal
    wave_out = np.zeros((len(signal), 2), dtype='float32')
    wave_out[:,0] = signal.real[:]
    wave_out[:,1] = signal.real[:]

    # Converting audio signal to float32 normalised
    wave_out_float = np.zeros((len(wave_out),2), dtype='float32')
    for i in range(len(wave_out)):
        wave_out_float[i] = wave_out.real[i]/32768.
    
    # Writing into a wav file
    wave.write('outputs/improved.wav', Fs, wave_out_float)
    return
