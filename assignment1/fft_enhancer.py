# -*- coding: utf-8 -*-
"""
Created on Thu Oct  4 11:04:49 2018
@author: Remy CHATEL 2411062C
@author: Chiemeka AGUWAMBA 2409785A
"""
import voice
import matplotlib.pyplot as plt

print("\n\n")
print("------------ Voice recording improvement ------------")
print("This program takes a recording of a voice and applies")
print("a series of modifications to it in order  to make it ")
print("sound better. The original file is \"original.wav\"  ")

plt.close('all')

data = voice.Voice("inputs/original.wav")
data.print("outputs/Original")

data.setDC(0)                       # Removing the DC component of the signal
data.cut_band(   15e3 , data.Fs/2)  # Low pass filter above the sound card cutoff
data.cut_band(     0  , 150      )  # High pass filter under 150Hz to remove bass noise
data.gaindB(-12,  325 , 350      )  
data.gaindB(- 3, 2.5e3, 4e3      )
data.gaindB(  6,  7e3 , 8e3      )

data.update()                       # Updating the time signal
data.print("outputs/Improved")              # Printing the figures
data.write("outputs/improved")              # Writing the improved signal to a file
plt.show()