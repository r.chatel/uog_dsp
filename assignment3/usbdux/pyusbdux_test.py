# load the module
import pyusbdux as dux

# open comedi
dux.open()

# Start data acquisition in the background: one channel, fs=250Hz
dux.start(1,250)

# Now we just read data at our convenience in a loop or timer or thread

# Let's check if samples are available
# if (dux.hasSampleAvilabale() == 0):
      # nope! Do something else or nothing

# Let's get a sample (array of all USB-DUX channels)
sample = dux.getSampleFromBuffer()

# do something with the sample, for example print it
print(sample)

# rinse and repeat!

# shutdown
dux.stop()
dux.close()
