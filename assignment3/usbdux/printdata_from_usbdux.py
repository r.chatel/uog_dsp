#!/usr/bin/python3
import numpy as np
import pyusbdux as dux
import matplotlib.pyplot as plt

result = dux.open()
if not result == 0:
    exit()

fs = 250

result = dux.start(8, fs)
if not result == 0:
    exit()

sample = np.array([])

t = 30
n = t * fs

while len(sample) < n:
    sample = np.append(sample, dux.getSampleFromBuffer()[0])
    print(len(sample))

np.savetxt('sample2.csv', sample)

plt.figure()
plt.plot(sample)
plt.show()
