#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Nov 28 11:38:17 2018

@author: remy
"""

import numpy as np
import matplotlib.pyplot as plt

data = np.loadtxt('sample2.csv')[:3000]
fs = 250

print(np.average(data))
data -= np.average(data)

plt.figure()
plt.plot(data)

data_fft = np.abs(np.fft.fft(data))
plt.figure()
plt.plot(np.linspace(0,fs/2, len(data)/2), data_fft[:int(len(data)/2)])
plt.show()