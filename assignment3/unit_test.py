# -*- coding: utf-8 -*-
"""
Created on Sat Nov 10 18:25:50 2018

@author: Remy CHATEL 2411062C
"""

# =============================================================================
# Tools
# =============================================================================
import numpy as np
import matplotlib.pyplot as plt
import scipy.signal as sg
import IIR as iir
plt.close('all')

def print_sig(signal, Fs, L):
    N = len(signal)
    t_axis = np.linspace(0, L, N)
    f_axis = np.linspace(0, Fs, N)
    sig_fft = np.abs(np.fft.fft(signal))
    
    plt.subplots(2,1, figsize=(6.4*1.5,4.8*1.5))
    plt.subplot(2,1,1)
    plt.plot(t_axis, signal.real)
    plt.xlabel("Time (s)")
    plt.ylabel("Data amplitude")
    plt.grid(True)
    plt.title("Time domain data", loc='left')
    
    plt.subplot(2,1,2)
    plt.plot(f_axis[:int(len(f_axis)/2)], sig_fft.real[:int(len(f_axis)/2)])
    plt.xlabel("Frequency (Hz)")
    plt.ylabel("Data amplitude")
    plt.yscale('log')
    plt.grid(True)
    plt.title("Frequency domain data", loc='left')
    return

# ==============================================================================
# Unit test on IIR2Filter using analytical definition of coeffs
# ==============================================================================
"""
Fs = 1000
f = 50 / Fs
r = 0.99
b = [1, -2*np.cos(2*np.pi*f)  , 1   ]
a = [1, -2*r*np.cos(2*np.pi*f), r**2]

z, p = sg.butter(2, f * 2)

filter1 = iir.IIR2Filter(b, a)
filter1.print()

duration = 8
n_sample = duration * Fs
t = np.linspace(0,duration, n_sample)
sig = np.sin(2*np.pi*50*t) + np.sin(2*np.pi*t) + 3

out = filter1.batch_filter(sig)

print_sig(sig, Fs, duration)
print_sig(out, Fs, duration)
# print_sig(out[int(len(out)/2):], Fs, duration/2)
"""
# =============================================================================
# Unit test on IIRFilter using high level functions for coeffs
# =============================================================================

Fs = 1000
Fc = 50
duration = 8
n_sample = duration * Fs
t = np.linspace(0,duration, n_sample)
sig = np.sin(2*np.pi*50*t) + np.sin(2*np.pi*t) + 3

f = Fc/Fs * 2
sos = sg.butter(8, [2*45/Fs, 2*55/Fs], output='sos', btype='bandstop')
print(sos)

filter2 = iir.IIRFilter(sos)
filter2.print()

out = filter2.batch_filter(sig)

# =============================================================================
# print_sig(sig, Fs, duration)
# print_sig(out, Fs, duration)
# =============================================================================

# =============================================================================
# Unit test with an ECG and butterworth
# =============================================================================
"""
dat = np.loadtxt('../assignment2/ecg_1.dat')[:,3]
Fs = 1000
duration = len(dat) / Fs

sos = sg.butter(4, [2*45/Fs, 2*55/Fs], output='sos', btype='bandstop')
filter2 = iir.IIRFilter(sos)
filter2.print()
sos = sg.butter(4, 1/Fs, output='sos', btype='highpass')
filter3 = iir.IIRFilter(sos)
filter3.print()
out = filter2.batch_filter(dat)
out = filter3.batch_filter(out)

print_sig(dat, Fs, duration)
print_sig(out, Fs, duration)
"""